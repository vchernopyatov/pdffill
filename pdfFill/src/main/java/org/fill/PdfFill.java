package org.fill;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

import java.io.*;
import java.util.List;

public class PdfFill {

    public static void main(String[] args) throws IOException {

        PDDocument pdf = PDDocument.load(PdfFill.class.getClassLoader().getResourceAsStream("form.pdf"));

        PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();

        PDFont font = PDType0Font.load(pdf, PdfFill.class.getClassLoader().getResourceAsStream("arial.ttf"));
        PDResources res = new PDResources();
        COSName fontName = res.add(font);
        form.setDefaultResources(res);
        String da = "/" + fontName.getName() + " 10 Tf 0 g";
        form.setDefaultAppearance(da);

        List<PDField> fields = form.getFields();

        for (PDField field : fields) {
            PDAcroForm acroForm = field.getAcroForm();
            acroForm.setDefaultResources(res);
            acroForm.setDefaultAppearance(da);
            field.setReadOnly(true);
        }

        fields.get(0).setValue("ООО Организация");

        form.flatten();

        pdf.save("C:\\Java\\ready_form.pdf");
        pdf.close();
    }
}
